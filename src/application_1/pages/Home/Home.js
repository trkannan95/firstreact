import React from 'react';
import style from './Home.module.css';
import Layout from '../../components/Layout/Layout';
import Button from '../../components/Button/Button';
import Avatar from '../../components/Avatar/Avatar';
import Heading1 from '../../components/typo/Heading1/Heading1';
import Heading2 from '../../components/typo/Heading2/Heading2';
import Paragraph from '../../components/typo/Paragraph/Paragraph';

export default class Home extends React.Component {
  render() {
    return (
      <div className={style.container}>
        <Layout>
          <div className={style.inner}>
            <div className={style.left}>
              <Avatar />
            </div>
            <div className={style.right}>
              <Heading1 className={style.h1}>SCARLETT JOHANSSON</Heading1>
              <Heading2 className={style.h2}>FRONTEND DEVELOPER</Heading2>
              <Paragraph className={style.para}>
                I implement user interface design and solve user problems with
                HTML, CSS and Javascript. I have 2 years of making products that
                solve user problems and Implementing responsive websites.
              </Paragraph>
              <Button>My Resume</Button>
            </div>
          </div>
        </Layout>
      </div>
    );
  }
}
