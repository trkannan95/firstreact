import React from 'react';
import Title from '../../components/Title/Title';
import style from './Projects.module.css';
import Layout from '../../components/Layout/Layout';
import Button from '../../components/Button/Button';

export default class Projects extends React.Component {
  render() {
    return (
      <div className={style.container}>
        <Layout>
          <Title title='Projects' heading='React' type='lower' />
          <div className={style.inner}>
            <div className={style.cardcontainer}>
              <div className={style.card}></div>
              <div className={style.card}></div>
              <div className={style.card}></div>
            </div>
            <div className={style.sep}>
              <Button palette='default'>More</Button>
            </div>
          </div>
        </Layout>
      </div>
    );
  }
}
