import React from 'react';
import Title from '../../components/Title/Title';
import style from './About.module.css';
import Layout from '../../components/Layout/Layout';
import Button from '../../components/Button/Button';
import Avatar from '../../components/Avatar/Avatar';
import Heading1 from '../../components/typo/Heading1/Heading1';
import Paragraph from '../../components/typo/Paragraph/Paragraph';

export default class About extends React.Component {
  render() {
    return (
      <div className={style.container}>
        <Layout>
          <Title title='Who am i' heading='about' palette='dark' />
          <div className={style.inner}>
            <div className={style.left}>
              <Avatar />
            </div>
            <div className={style.right}>
              <Paragraph className={style.para}>
                I implement user interface design and solve user problems with
                HTML, CSS and Javascript. I have 2 years of making products that
                solve user problems and Implementing responsive websites.
              </Paragraph>
              <Heading1 className={style.h1}>Skills</Heading1>
              <div className={style.btngroup}>
                <Button>HTML</Button>
                <Button>CSS</Button>
                <Button>Javascript</Button>
                <Button>Bootstrap</Button>
                <Button>PHP</Button>
                <Button>Python</Button>
                <Button>C++</Button>
                <Button>Wordpress</Button>
              </div>
            </div>
          </div>
        </Layout>
      </div>
    );
  }
}
