import React from 'react';
import Title from '../../components/Title/Title';
import style from './Contact.module.css';
import Layout from '../../components/Layout/Layout';
import Button from '../../components/Button/Button';
import TextBox from '../../components/form/TextBox/TextBox';
import Textarea from '../../components/form/Textarea/Textarea';

export default class Contact extends React.Component {
  render() {
    return (
      <div className={style.container}>
        <Layout>
          <Title title='Get In Touch' heading='Contact' />
          <div className={style.inner}>
            <TextBox placeholder='Name' className={style.gap1} />
            <TextBox placeholder='Email' className={style.gap1} />
            <Textarea placeholder='Message' className={style.gap2}></Textarea>
            <Button>SEND MESSAGE</Button>
          </div>
        </Layout>
      </div>
    );
  }
}
