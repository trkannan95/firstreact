import Header from './components/Header/Header';
import Content from './components/Content/Content';
import style from './App.module.css';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <div className={style.container}>
      <BrowserRouter>
        <Header />

        <Content />
      </BrowserRouter>
    </div>
  );
}

export default App;
