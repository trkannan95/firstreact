import React from 'react';
import style from './Avatar.module.css';
import image from '../../../images/avatar.jpeg';

export default class Avatar extends React.Component {
  render() {
    let { className } = this.props;
    return (
      <img
        className={`${style.container}  ${className ? className : ''}`}
        alt='avatar'
        src={image}
      />
    );
  }
}
