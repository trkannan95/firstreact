import React from 'react';
import style from './Header.module.css';
import Layout from '../Layout/Layout';
import { Link, useLocation } from 'react-router-dom';

const Header = () => {
  let location = useLocation().pathname;
  return (
    <div className={`${style.container} ${style.header}`}>
      <Layout className={`${style.header}`}>
        <div className={style.left}>
          <Link to='/'>SJ.</Link>
        </div>
        <div className={style.right}>
          <ul>
            <li>
              <Link
                to='/home'
                className={
                  location === '/home' || location === '/' ? style.active : ''
                }
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                to='/projects'
                className={location === '/projects' ? style.active : ''}
              >
                Projects
              </Link>
            </li>
            <li>
              <Link
                to='/about'
                className={location === '/about' ? style.active : ''}
              >
                About
              </Link>
            </li>
            <li>
              <Link
                to='/contact'
                className={location === '/contact' ? style.active : ''}
              >
                Contacts
              </Link>
            </li>
          </ul>
        </div>
      </Layout>
    </div>
  );
};

export default Header;
