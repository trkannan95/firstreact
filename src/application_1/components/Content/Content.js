import React from 'react';
import style from './Content.module.css';
import About from '../../pages/About/About';
import Home from '../../pages/Home/Home';
import Contact from '../../pages/Contact/Contact';
import Projects from '../../pages/Projects/Projects';
import { Routes, Route } from 'react-router-dom';

export default class Content extends React.Component {
  render() {
    return (
      <div className={style.container}>
        <Routes>
          <Route path='home' element={<Home />} />
          <Route path='about' element={<About />} />
          <Route path='projects' element={<Projects />} />
          <Route path='contact' element={<Contact />} />
          <Route path='/' element={<Home />} />
        </Routes>
      </div>
    );
  }
}
