import React from 'react';
import style from './Textarea.module.css';

export default class Textarea extends React.Component {
  render() {
    let { className, placeholder = '.' } = this.props;
    return (
      <textarea
        className={`${style.container}  ${className ? className : ''}`}
        placeholder={placeholder}
      ></textarea>
    );
  }
}
