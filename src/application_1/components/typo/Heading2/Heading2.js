import React from 'react';
import style from './Heading2.module.css';

export default class Heading2 extends React.Component {
  render() {
    let { children, className } = this.props;
    return (
      <div className={`${style.container}  ${className ? className : ''}`}>
        {children}
      </div>
    );
  }
}
