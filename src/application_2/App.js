import React from 'react';
import Header from './components/Header/Header';
import Content from './components/Content/Content';
import style from './App.module.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.changePage = this.changePage.bind(this);
    this.state = { pageName: 'home' };
  }
  changePage(page) {
    this.setState({
      pageName: page
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    let { pageName } = this.state;
    if (nextState.pageName !== pageName) {
      return true;
    } else {
      return false;
    }
  }
  render() {
    let { pageName } = this.state;
    return (
      <div className={style.container} id='application'>
        <Header pageName={pageName} onPageChange={this.changePage} />

        <Content pageName={pageName} />
      </div>
    );
  }
}
