import React from 'react';
import style from './Button.module.css';

export default class Button extends React.Component {
  render() {
    let { children, palette = 'primary' } = this.props;
    return (
      <button type='button' className={`${style.container} ${style[palette]}`}>
        {children}
      </button>
    );
  }
}
