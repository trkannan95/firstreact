import React from 'react';
import style from './Header.module.css';
import Layout from '../Layout/Layout';
// import { Link, useLocation } from 'react-router-dom';
const homeLink='https://awesomebk1.netlify.app#';
const Header = props => {
  // let location = useLocation().pathname;
  let { onPageChange, pageName } = props;

  function changePage(page) {
    onPageChange(page);
    //console.log('asdfs');
  }
  return (
    <div className={`${style.container} ${style.header}`}>
      <Layout className={`${style.header}`}>
        <div className={style.left}>
          {/* <Link to='/'>SJ.</Link> */}
          <a
            href={homeLink}
            onClick={changePage.bind(null, 'home')}
          >
            SJ.
          </a>
        </div>
        <div className={style.right}>
          <ul>
            <li>
              <a
                href={homeLink}
                onClick={changePage.bind(null, 'home')}
                className={pageName === 'home' ? style.active : ''}
              >
                Home
              </a>
            </li>
            <li>
              <a
                href={homeLink}
                onClick={changePage.bind(null, 'projects')}
                className={pageName === 'projects' ? style.active : ''}
              >
                Projects
              </a>
            </li>
            <li>
              <a
                href={homeLink}
                onClick={changePage.bind(null, 'about')}
                className={pageName === 'about' ? style.active : ''}
              >
                About
              </a>
            </li>
            <li>
              <a
                href={homeLink}
                onClick={changePage.bind(null, 'contact')}
                className={pageName === 'contact' ? style.active : ''}
              >
                Contacts
              </a>
            </li>
          </ul>
        </div>
      </Layout>
    </div>
  );
};

export default Header;
