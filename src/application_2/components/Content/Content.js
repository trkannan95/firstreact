import React from 'react';
import style from './Content.module.css';
import About from '../../pages/About/About';
import Home from '../../pages/Home/Home';
import Contact from '../../pages/Contact/Contact';
import Projects from '../../pages/Projects/Projects';
// import { Routes, Route } from 'react-router-dom';

export default class Content extends React.Component {
  constructor(props) {
    super(props);
    this.renderPage = this.renderPage.bind(this);
  }

  renderPage(page) {
    if (page === 'home') {
      return <Home />;
    } else if (page === 'about') {
      return <About />;
    } else if (page === 'contact') {
      return <Contact />;
    } else if (page === 'projects') {
      return <Projects />;
    } else {
      return <Home />;
    }
  }

  render() {
    let { pageName = 'home' } = this.props;

    return <div className={style.container}>{this.renderPage(pageName)}</div>;
  }
}
