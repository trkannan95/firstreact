import React from 'react';
import style from './Paragraph.module.css';

export default class Paragraph extends React.Component {
  render() {
    let { children, className } = this.props;
    return (
      <p className={`${style.container}  ${className ? className : ''}`}>
        {children}
      </p>
    );
  }
}
