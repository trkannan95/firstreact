import React from 'react';
import style from './Content.module.css';
import About from '../../pages/About/About';
import Home from '../../pages/Home/Home';
import Contact from '../../pages/Contact/Contact';
import Projects from '../../pages/Projects/Projects';
// import { Routes, Route } from 'react-router-dom';

export default class Content extends React.Component {
  constructor(props) {
    super(props);
    this.renderPage = this.renderPage.bind(this);
    this.getleftPosition = this.getleftPosition.bind(this);
    this.state={left:'0px'}
  }

  renderPage(page) {
    if (page === 'home') {
      return <Home />;
    } else if (page === 'about') {
      return <About />;
    } else if (page === 'contact') {
      return <Contact />;
    } else if (page === 'projects') {
      return <Projects />;
    } else {
      return <Home />;
    }
  }

  getleftPosition(page)
  {
    if (page === 'home') {
      return '-0%';
    } else if (page === 'about') {
      return '66.666%';
    } else if (page === 'contact') {
      return '99.999%';
    } else if (page === 'projects') {
      return '33.333%';
    } else {
      return '-0%';
    }
  }

  render() {
    let { pageName = 'home' } = this.props;

    return <div className={style.container}>
      <div className={style.ribbon} style={{backgroundPositionX:this.getleftPosition(pageName)}}>
    
      </div>
      {this.renderPage(pageName)}
      </div>;
  }
}
