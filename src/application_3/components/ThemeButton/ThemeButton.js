import React from 'react';
import style from './ThemeButton.module.css';
import {ThemeCofigConsumer} from '../Context';

export default class ThemeButton extends React.Component {
  constructor(props)
  {
    super(props);
   // this.buttonElement= React.createRef();
    this.state={boxShadow:''}
  }
  componentDidMount()
  {
    // let {boxShadow}=this.state;
    let winWidth=window.innerWidth;
    let winHeight=window.innerHeight;
    let gap=60;
    let boxShadowText=[];
    for (let i=gap; i<=winWidth-gap*3;i=i+gap)
    {
      for (let j=gap; j<=winHeight-gap*2;j=j+gap)
        {
          boxShadowText.push('-'+i+'px -'+j+'px 0px var(--theme_shadow)');
        }
    }
    this.setState({boxShadow:boxShadowText.join(',')});
  }

  render() {
    let {boxShadow}=this.state;
    return (
      <ThemeCofigConsumer>
        {
          (config)=>{
            let {mode,onChangeMode}=config;
            return (
              <>
              <style>
                {'.'+style.container+':hover{box-shadow:'+boxShadow+'}'}
              </style>
      <button  type='button' onClick={onChangeMode} className={`${style.container} ${mode==='dark'?style.dark:style.light}`} >
       {mode==='dark'?<>&#9788;</>:<>&#9789;</>}
      </button></>);
          }
  }
      </ThemeCofigConsumer>
    );
  }
}
