import React from 'react';

let ThemeConfig = React.createContext({mode:'dark',onChangeMode:()=>{}});
let ThemeCofigProvider=ThemeConfig.Provider;
let ThemeCofigConsumer=ThemeConfig.Consumer;

export {ThemeConfig,ThemeCofigProvider,ThemeCofigConsumer};