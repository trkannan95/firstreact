import React from 'react';
import style from './Title.module.css';

export default class Title extends React.Component {
  render() {
    let { title, heading, type = 'default', palette = 'light' } = this.props;
    return (
      <div className={`${style.container} ${style[type]}  ${style[palette]}`}>
        <div className={style.title}>{title}</div>
        <div className={style.heading}>{heading}</div>
      </div>
    );
  }
}
