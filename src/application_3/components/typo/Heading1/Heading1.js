import React from 'react';
import style from './Heading1.module.css';

export default class Heading1 extends React.Component {
  render() {
    let { children, className } = this.props;
    return (
      <div className={`${style.container}  ${className ? className : ''}`}>
        {children}
      </div>
    );
  }
}
