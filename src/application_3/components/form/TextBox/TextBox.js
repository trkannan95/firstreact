import React from 'react';
import style from './TextBox.module.css';

export default class TextBox extends React.Component {
  render() {
    let { className, placeholder = '.' } = this.props;
    return (
      <input
        type='text'
        className={`${style.container}  ${className ? className : ''}`}
        placeholder={placeholder}
      />
    );
  }
}
