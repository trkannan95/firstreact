import React from 'react';
import style from './Layout.module.css';
export default class Layout extends React.Component {
  render() {
    let { children, className } = this.props;
    return (
      <div className={`${style.container} ${className ? className : ''}`}>
        {children}
      </div>
    );
  }
}
