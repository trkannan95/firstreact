import React from 'react';
import Header from './components/Header/Header';
import Content from './components/Content/Content';
import style from './App.module.css';
import themeStyle from './components/theme.module.css';
import ThemeButton from './components/ThemeButton/ThemeButton';
import {ThemeCofigProvider,ThemeCofigConsumer} from './components/Context/index';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.changePage = this.changePage.bind(this);
    this.changeTheme=this.changeTheme.bind(this);
    this.state = { pageName: 'home' , mode:'dark'};
  }
  changePage(page) {
    this.setState({
      pageName: page
    });
  }
  changeTheme()
  {
    this.setState(prevState=>{
      return {mode:prevState.mode==='light'?'dark':'light'};
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    let { pageName,mode } = this.state;
    if (nextState.pageName !== pageName || nextState.mode!==mode) {
      return true;
    } else {
      return false;
    }
  }
  render() {
    let { pageName,mode} = this.state;
    return (
      <ThemeCofigProvider value={{mode:mode,onChangeMode:this.changeTheme}}>
        <ThemeCofigConsumer>
          {
            (config)=>{
              let {mode}=config;
              return ( <div className={`${style.container} ${mode==='light'?themeStyle.light:themeStyle.dark}`} id='application'>
              <Header pageName={pageName} onPageChange={this.changePage} />
      
              <Content pageName={pageName} />
              <ThemeButton/>
            </div>);
            }
     
  }
      </ThemeCofigConsumer>
      </ThemeCofigProvider>
    );
  }
}
